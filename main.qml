import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Dialogs 1.0
import QtQuick.Controls.Universal 2.0
import Qt.labs.settings 1.0

ApplicationWindow {
    id: window
    width: 1280
    height: 720
    minimumWidth: 1280
    minimumHeight: 720
    title: "SabtShod"
    visible: true
    Material.theme: Material.Light
    FontLoader { id: iransansBold; source: "font/iransans/iransans-bold.ttf"}
    FontLoader { id: iransansLight; source: "font/iransans/iransans-light.ttf"}
    FontLoader { id: sanfranciscoRegular; source: "font/sanfrancisco/sanfrancisco-regular.ttf"}
    FontLoader { id: sanfranciscoBold; source: "font/sanfrancisco/sanfrancisco-bold.ttf"}
    Connections {
        target: qmlBridge
        onSendToQml:
        {
            hashColumn.hash = data
            hashColumn.copyBtn = true

        }
        onSendError:
        {

        }
    }


    header: ToolBar {
        id: header
        height: 0.11 * window.height
        Material.foreground: "white"
        Material.primary: "#ffffff"
        RowLayout {
            id: headLayout
            anchors.fill: parent
            anchors.centerIn: header.anchors.centerIn
            Image {
                id: sabtLogo
                source: "images/logo.png"
                sourceSize.width: 0.1 * parent.width
                sourceSize.height: 0.5 * parent.height
                anchors.right: parent.right
                anchors.rightMargin: 0.45 * parent.width
            }
            Label {
                id: sabtText
                text: "ثبت شد"
                anchors.right: sabtLogo.left
                anchors.rightMargin: 0.01 * parent.width
                color: "#64cb63"
                font { family: iransansBold.name; pixelSize: Math.max((window.height * window.width * 29)/(2073600), 25); bold: true}
            }
        }
    }
    StackView {
        id: stackView
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        Material.background: "#ffffff"
        initialItem: Pane {
            Pane {

                id: hashPan
                Material.elevation: 5
                Material.background: "#ffffff"
                width: 0.33 * window.width
                height: 0.71 * window.height
                anchors.right: parent.right
                anchors.rightMargin: 0.092 * window.width
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0.087 * window.height
                Column {
                    id: hashColumn
                    property bool copyBtn: false
                    property string hash
                    width: parent.width
                    height: parent.height
                    anchors.centerIn: parent.Center

                    Label {
                        id: topLabel
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 0.057 * parent.height
                        text: "رمزگذاری اثر به صورت آفلاین"
                        font { family: iransansBold.name; pixelSize: Math.max((window.height * window.width * 29)/(2073600), 20); bold: true}
                    }

                    Rectangle {
                        id: hashRect
                        property bool file: false
                        property bool progress : false
                        readonly property int itemWidth: Math.max(bar.implicitWidth, hashRect.availableWidth / 3)

                        width: 0.6 * parent.width
                        height: 0.397 * parent.height
                        radius: 5
                        border.color: "#000000"
//                        color: Material.Grey
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 0.245 * parent.height

                        DropArea {
                            id: dropArea

                            width: parent.width
                            height: parent.height
                            onDropped:
                            {
                                hashRect.progress = true
                                console.log(drop.text.trim().toString())
                                qmlBridge.sendToGo(drop.text.trim().toString().substring(0, drop.text.trim().toString().length))
                            }
                        }
                        Image {
                            id: cloudImg

                            source: "images/cloud.png"

                            sourceSize.width: 0.156 * parent.width
                            sourceSize.height: 0.164 * parent.height

                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 0.708 * parent.height
                        }
                        Label {
                            id: hashLabel

                            text: "فایل خود را انتخاب کنید یا به اینجا بکشید"
                            font { family: iransansBold.name; pixelSize: Math.max((window.height * window.width * 20)/(2073600), 14); bold: true}

                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 0.484 * parent.height

                        }



                        Button {
                            id: selectButton

                            width: 0.487 * parent.width
                            height: 0.15 * parent.height
                            text: "آپلود فایل"
                            font { family: iransansLight.name; pixelSize: Math.max((window.height * window.width * 15)/(2073600), 10)}
                            Material.accent: "#3b86ff"
                            highlighted: true
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 0.219 * parent.height
                            onClicked: {
                                fileDialog.open()
                            }
                        }



                    }
                    Label {
                        id: bottomLabel
                        wrapMode: Label.Wrap

                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 0.678 * parent.height

                        text: "رمز اثر"
                        font { family: iransansBold.name; pixelSize: Math.max((window.height * window.width * 20)/(2073600), 14); bold: true}

                    }

                    TextField {
                        id: hashField

                        text: hashColumn.hash
                        font { family: sanfranciscoRegular.name; pixelSize: Math.max((window.height * window.width * 15)/(2073600), 10)}
                        readOnly: true
                        selectByMouse: true
                        width: 0.602 * parent.width
                        height: Math.max(0.059 * parent.height, 35)

                        Material.accent: "#3b86ff"
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 0.748 * parent.height

                    }

                    Button {
                        id: copyButton

                        width: 0.602 * parent.width
                        height: 0.09 * parent.height
                        text: "کپی رمز"
                        font { family: iransansLight.name; pixelSize: Math.max((window.height * window.width * 15)/(2073600), 10)}
                        Material.accent: "#00cb62"
                        highlighted: true
                        enabled: hashColumn.copyBtn
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 0.842 * parent.height
                        onClicked: {
                            hashField.selectAll()
                            hashField.copy()
                            hashField.deselect()
                        }
                    }



                }

                }
            Image {
                id: cypherPic
                source: "images/chipher.png"

                anchors.right: hashPan.left
                anchors.rightMargin: 0.06 * window.width
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0.05 * window.height

                sourceSize.width: 0.644 * window.width
                sourceSize.height: 0.98 * window.height
            }

            FileDialog {
                id: fileDialog

                selectMultiple: false
                selectFolder: false
                onFolderChanged: {

                }

                onAccepted: {
                    console.log(fileDialog.fileUrl)
                    hashRect.progress = true

                    qmlBridge.sendToGo(fileDialog.fileUrl.toString().substring(0, fileDialog.fileUrl.toString().length))
                    close()
                }
                onRejected: {
                    close()
                }
            }

        }
    }

}

