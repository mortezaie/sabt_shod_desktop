package main

import (
	"crypto/sha256"
	"encoding/base64"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/qml"
	"github.com/therecipe/qt/quickcontrols2"
	"io/ioutil"
	"os"
	"runtime"
	"strings"
)

var (
	qmlObjects = make(map[string]*core.QObject)
)

type add struct {
	address string `json:"address"`
}

type QmlBridge struct {
	core.QObject

	_ func(data string) `signal:"sendToQml"`
	_ func(data string) `slot:"sendToGo"`
	ـ func(err string)  `signal:"sendError"`
}

var (
	qmlBridge *QmlBridge
)

func main() {
	qmlBridge = NewQmlBridge(nil)

	g := gui.NewQGuiApplication(len(os.Args), os.Args)
	// icon := gui.NewQIcon5("qrc:/images/sabt_shod_desktop.icns")
	// g.SetWindowIcon(icon)
	q := qml.NewQQmlApplicationEngine(g)
	quickcontrols2.QQuickStyle_SetStyle("Material")
	InitQmlBridge(qmlBridge)
	q.RootContext().SetContextProperty("qmlBridge", qmlBridge)
	q.Load(core.NewQUrl3("qrc:/main.qml", 0))

	defer func() {
		if r := recover(); r != nil {

		}
	}()

	g.Exec()
}

func InitQmlBridge(qmlBridge *QmlBridge) {
	qmlBridge.ConnectSendToGo(standardFileADD)
}

func sendErrorToQml(errString string) {
	qmlBridge.SendError(errString)
}

func standardFileADD(data string) {
	sha, err := generateSHA256(data)
	if err != nil {
		sendErrorToQml("در حال حاضر نرم‌افزار با مشکل مواجه شده است.")
	}
	qmlBridge.SendToQml(sha)
}

func generateSHA256(url string) (string, error) {
	var data []byte
	var err error

	if runtime.GOOS == "linux" {
		data, err = ioutil.ReadFile(strings.Replace(url, "file://", "", -1))
	} else if runtime.GOOS == "darwin" {
		data, err = ioutil.ReadFile(strings.Replace(url, "file://", "", -1))
	} else {
		data, err = ioutil.ReadFile(strings.Replace(url, "file:///", "", -1))
	}
	if err != nil {
		return "", err
	}
	hash := sha256.New()
	hash.Write(data)
	str := base64.StdEncoding.EncodeToString(hash.Sum(nil))
	hash.Reset()
	return str, nil
}
